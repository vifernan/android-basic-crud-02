package com.example.semana9;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class DetallePostulanteActivity extends AppCompatActivity {

    Button fotoButton;
    ImageView imgView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_postulante);

        // Obtener los datos pasados desde la Activity principal
        int id = getIntent().getIntExtra("id", 0);
        String nombre = getIntent().getStringExtra("nombre");
        String apellido = getIntent().getStringExtra("apellido");
        String universidad = getIntent().getStringExtra("universidad");

        // Mostrar los datos en los TextViews correspondientes
        TextView tvNombre = findViewById(R.id.nombre);
        TextView tvApellido = findViewById(R.id.apellido);
        TextView tvUniversidad = findViewById(R.id.universidad);

        tvNombre.setText(nombre);
        tvApellido.setText(apellido);
        tvUniversidad.setText(universidad);

        Button btnCerrar = findViewById(R.id.btnVolver);
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Crear un intent para volver a la MainActivity
                Intent intent = new Intent(DetallePostulanteActivity.this, MainActivity.class);
                // Iniciar la MainActivity
                startActivity(intent);
                // Cerrar la actividad actual (DetallePostulanteActivity)
                finish();
            }
        });

        Button fotoButton = (Button)findViewById(R.id.btnFoto);
        imgView = (ImageView)findViewById(R.id.ivImagen);

        fotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent (MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 0);
            }
        });

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap= (Bitmap) data.getExtras().get("data");
        imgView.setImageBitmap(bitmap);
    }

}
